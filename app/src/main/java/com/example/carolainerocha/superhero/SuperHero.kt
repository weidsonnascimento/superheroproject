package com.example.carolainerocha.superhero

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
@Entity
data class SuperHero (@PrimaryKey (autoGenerate = true)var id: Long, var name: String, var publisher: String){}