package com.example.carolainerocha.superhero
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class ConnectionStatus {

    companion object {
        //Verificando se está conectado
        fun isConnected(ctx: Context): Boolean {
            //Pegando serviço de conectividade
            val con = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val net = con.allNetworks
            for (n in net) {
                val info = con.getNetworkInfo(n)
                if (info.state == NetworkInfo.State.CONNECTED) {
                    return true
                }
            }
            return false
        }
    }
}