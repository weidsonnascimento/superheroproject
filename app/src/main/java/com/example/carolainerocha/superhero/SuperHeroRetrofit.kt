package com.example.carolainerocha.superhero

import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.example.carolainerocha.superhero.SuperHero

class SuperHeroRetrofit {
    companion object {

       //Montando requisicao do retrofit
        val retrofit = Retrofit.Builder()
                //Url completa
                .baseUrl("http://superheroapi.com/api/1848326485222745/644")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        //Acesso a interface
        val superHero = retrofit.create(SuperHeroRetrofitInterface::class.java)


        fun getSuperHero(call: Callback<SuperHero>) {
            //Adicionando a fila de requisição
            superHero.getSuperHero().enqueue(call)
        }

    }
}