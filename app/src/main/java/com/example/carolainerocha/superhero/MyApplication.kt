package com.example.carolainerocha.superhero

import android.app.Application
import android.arch.persistence.room.Room
import com.example.carolainerocha.superhero.SuperHeroDataBase

class MyApplication : Application(){

    companion object {
        //Verificando se existe db
        var database: SuperHeroDataBase? = null
    }

    override fun onCreate() {
        super.onCreate()

        //Start Database
        database = Room.databaseBuilder(this, SuperHeroDataBase::class.java, "Db_SuperHero").allowMainThreadQueries().build()

    }
}