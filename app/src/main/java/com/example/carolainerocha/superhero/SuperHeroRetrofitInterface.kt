package com.example.carolainerocha.superhero

import com.example.carolainerocha.superhero.SuperHero
import retrofit2.Call
import retrofit2.http.GET

interface SuperHeroRetrofitInterface {

    @GET("superhero")
    fun getSuperHero() : Call<SuperHero>
}