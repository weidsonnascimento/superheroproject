

package com.example.carolainerocha.superhero

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_super_hero.*
import com.example.carolainerocha.superhero.R
import com.example.carolainerocha.superhero.MyApplication
import com.example.carolainerocha.superhero.SuperHeroRetrofit
import com.example.carolainerocha.superhero.ConnectionStatus
import retrofit2.Response
import retrofit2.Callback
import retrofit2.Call
import com.example.carolainerocha.superhero.SuperHero

class SuperHeroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_super_hero)

        if (ConnectionStatus.isConnected(this)) {
            //Conectado
            Log.e("test", "conectado")
            callBackSuperHero()
        } else {
            Log.e("test", "desconectado")
            getSuperHeroDataBase()
        }
    }

    fun callBackSuperHero() {

        val callBack = object : Callback<SuperHero> {
            override fun onResponse(call: Call<SuperHero>?, response: Response<SuperHero>?) {

                val superHero = response?.body()

                if (superHero != null) {
                    //Montar heroi na tela.
                    buildSuperHero(superHero)
                }
            }

            override fun onFailure(call: Call<SuperHero>?, t: Throwable?) {
                //Pegar do banco aqui
                getSuperHeroDataBase()
            }
        }

        SuperHeroRetrofit.getSuperHero(callBack)

    }

    fun buildSuperHero(superHero: SuperHero) {

        textView.text = superHero?.name
        textView2.text = superHero?.publisher

        insertSuperHero(superHero);
    }

    fun insertSuperHero(superHero: SuperHero) {

        MyApplication.database?.superHeroDaoInterface()?.addSuperHero(superHero)
    }

    fun getSuperHeroDataBase() {

        val objSuperHero = MyApplication.database?.superHeroDaoInterface()?.getSuperHero()

        if (objSuperHero != null) {

            buildSuperHero(objSuperHero?.get(0))
            Toast.makeText(this@SuperHeroActivity, "Consultou" , Toast.LENGTH_LONG).show()

        }

    }

}
