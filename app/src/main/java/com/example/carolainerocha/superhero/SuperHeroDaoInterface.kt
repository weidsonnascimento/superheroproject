package com.example.carolainerocha.superhero

import android.arch.persistence.room.Insert
import android.arch.persistence.room.Dao
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.carolainerocha.superhero.SuperHero

@Dao
interface SuperHeroDaoInterface {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addSuperHero(vararg superHero: SuperHero)

    @Query("SELECT * FROM SuperHero")
    fun getSuperHero() : List<SuperHero>
}