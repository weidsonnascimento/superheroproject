package com.example.carolainerocha.superhero

import com.example.carolainerocha.superhero.SuperHero
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = arrayOf(SuperHero::class), version = 1 ,exportSchema = false)
abstract class SuperHeroDataBase : RoomDatabase() {

    abstract fun superHeroDaoInterface(): SuperHeroDaoInterface

}